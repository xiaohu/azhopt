import logging
import sys

def sure_to_call(phrase):
    if len(sys.argv) < 2:
        print('Not sure you want to run the script. Exit.')
        exit(0)
    if sys.argv[1] == phrase:
        return
    else:
        print('Wrong phrase. Are you sure to run the script? Exit.')
        exit(0)

def setup_custom_logger(name):
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler('log.{0}'.format(name), mode='w')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger
