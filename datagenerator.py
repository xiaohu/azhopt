import numpy as np
from sklearn.utils import shuffle
from xutil import *
import keras

class DataGenerator(keras.utils.Sequence):
    'Generate data for nn, use all signal, sample background with similar size of signals'

    # require Y as type int
    # assume nsig is much less than nbkg in training samples
    def __init__(self, name, X, Y, batch_size=64, shuffle_per_epoch=True):
        self.logger = setup_custom_logger( 'dg_{0}'.format(name) )

        self.name = name
        self.X = X
        self.Y = Y
        self.X_sig_idx = (Y==1)
        self.X_bkg_idx = (Y==0)
        self.nsig = int(Y.astype('int').sum())
        self.nbkg = int(Y.shape[0] - self.nsig)
        self.batch_size = int(batch_size)
        self.nbatch = int(np.floor( self.nsig / (self.batch_size/2) ))
        self.nepoch = int(np.floor( self.nbkg / self.nsig ))
        self.shuffle_per_epoch = shuffle_per_epoch
        self.bkg_idx_rnd_list = None # in on_epoch_end()
        self.epoch_i = 0

        self.logger.info('DataGenerator: name = {0}'.format(self.name))
        self.logger.info('nsig {0} nbkg {1}'.format(self.nsig, self.nbkg))
        self.logger.info('nbatch {0} nepoch {1} <------- recommended minimal'.format(self.nbatch, self.nepoch))
        self.logger.info('batch size {0}'.format(self.batch_size))
        self.logger.info('shuffle per epoch {0}'.format(self.shuffle_per_epoch))

        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.nbatch

    def __getitem__(self, index):
        'Generate one batch of data'
        # generate indexes
        # np.random seems not multiprocess safe, found the same randint output in multiprocess
        # set randseed to process dependent ... not seem working ...
        #sig_idx = np.random.randint( self.nsig, size=self.batch_size/2 )
        #state_per_batch = np.random.RandomState( index*2018 )
        #bkg_idx = state_per_batch.randint( int(self.nbkg), size=self.batch_size/2 )
        # Thus, prepare bkg sampling in on_epoch_end with a single process !

        #sig_sel = self.X[ self.X_sig_idx, : ][ sig_idx, : ]
        # nsig >= batch_size/2 * nbatch, take batch_size/2 per batch
        sig_sel = self.X[ self.X_sig_idx, : ][ index*self.batch_size//2:(index+1)*self.batch_size//2, : ]
        #bkg_sel = self.X[ self.X_bkg_idx, : ][ bkg_idx, : ]
        bkg_sel = self.X[ self.X_bkg_idx, : ][ self.bkg_idx_rnd_list[index], : ]

        # interweave
        X = np.empty( (sig_sel.shape[0]+bkg_sel.shape[0], sig_sel.shape[1]), dtype=sig_sel.dtype )
        X[0::2,] = sig_sel
        X[1::2,] = bkg_sel

        Y = np.empty( sig_sel.shape[0]+bkg_sel.shape[0], dtype='int' )
        #Y[0::2] = 1
        #Y[1::2] = 0
        Y[0::2] = self.Y[ self.X_sig_idx ][ index*self.batch_size//2:(index+1)*self.batch_size//2 ]
        Y[1::2] = self.Y[ self.X_bkg_idx ][ self.bkg_idx_rnd_list[index] ]

        # debug
        #if index == 0 or index == 1:
        #    self.logger.info('Batch idx {0}'.format(index))
        #    print 'bkg_idx', self.bkg_idx_rnd_list[index]
        #    print 'sig_sel', sig_sel.shape#, sig_sel
        #    print 'bkg_sel', bkg_sel.shape#, bkg_sel
        #    #print X, Y
        #    print X.shape, Y.shape

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        
        # generate relative background idx (after [self.X_bkg_idx]) on single process
        _rnd_state = np.random.RandomState( 201811+self.epoch_i*33 ) # epoch dependence
        _idx_choose = _rnd_state.choice( np.arange( self.nbkg, dtype='int' ), size=self.batch_size//2*self.nbatch, replace=False ) # unique idx
        # nbkg >= nepoch * nbatch * batch_size
        self.bkg_idx_rnd_list = _idx_choose.reshape( self.nbatch, self.batch_size//2 )

        if self.shuffle_per_epoch == True :
            self.X, self.Y = shuffle( self.X, self.Y )

        # my count
        self.epoch_i += 1 # indexing from 0, this is the end of epoch, thus only next epoch see it

    def get_min_nepoch_recommended(self):
        return self.nepoch
