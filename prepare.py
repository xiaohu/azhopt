# NOW this script is included in add_variable.py
# prepare dataset, label s,b, remove rows (samples) etc.
# python prepare.py outputfile.pickle

import argparse
import os
import re
from glob import glob
import numpy as np
import pandas as pd
import sys
import pickle

### CONFIG ###
# this file contains all samples
thefile = "/eos/atlas/user/x/xiaohu/public/AZH_Optimisation/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy"
# can try pickle now ... 20181001_prefix-pNN_input_2tag_Sig_BKGs.pickle
### CONFIG ###

# system
import xutil
logger = xutil.setup_custom_logger('logger_prepare')

if len(sys.argv) < 2:
  print('Usage: python prepare.py outputfile.pickle')
  sys.exit()

# load in
logger.info('Loading ... {}'.format(thefile))
myarr = np.load(thefile)
logger.info('Loading done, now feed to df')
df = pd.DataFrame(data=myarr, columns=["label","weight",
                                       "lep0pt","lep0eta","lep0phi",
                                       "lep1pt","lep1eta","lep1phi",
                                       "jet0pt","jet0eta","jet0phi",
                                       "jet1pt","jet1eta","jet1phi",
                                       "jet2pt","jet2eta","jet2phi",
                                       "MET","METSig","NBJet","NEvent"])
logger.info('Print df summary:')
#print(df.describe(include='all'))
print(df)

# 1. remove large width samples by labels
logger.info("1. Remove large width samples by labels")
drop1 = df.index[ df["label"] == 306975  ].tolist()  #ggA:500,150,LW05
drop2 = df.index[ df["label"] == 306976  ].tolist()  #ggA:500,150,lW10
drop3 = df.index[ df["label"] == 306977  ].tolist()  #ggA:500,150,LW20
drop4 = df.index[ df["label"] == 306978  ].tolist()  #ggA:600,300,LW05
drop5 = df.index[ df["label"] == 306979  ].tolist()  #ggA:600,300,LW10
drop6 = df.index[ df["label"] == 306980  ].tolist()  #ggA:600,300,LW20
drop7 = df.index[ df["label"] == 306981  ].tolist()  #ggA:700,400,LW05
drop8 = df.index[ df["label"] == 306982  ].tolist()  #ggA:700,400,LW10
drop9 = df.index[ df["label"] == 306983  ].tolist()  #ggA:700,400,LW20

logger.info('#evt before drop LW: {}'.format(len(df)) )
drop = drop1+drop2+drop3+drop4+drop5+drop6+drop7+drop8+drop9
df = df.drop( df.index[drop]  )
logger.info('#evt to remove: {}'.format(len(drop)) )
logger.info('#evt after drop LW: {}'.format(len(df)) )

# 2. add column EventNumber
logger.info('2. Add EventNumber (need to use real ones in future!)')
nframes = len(df)
# add event number  -- in the future we will need to use NEvent instead !!! @Liverpool
EventNumber = np.array( [ i for i in range(nframes)    ] )
df["EventNumber"] = EventNumber

# 3. add columne mA mH
logger.info("3. Add columnes mA,mH for signal")
df["mA"] = np.zeros(nframes)
df["mH"] = np.zeros(nframes)
df.loc[ df.label == 306939, ["mA", "mH"]  ] = 230,130
df.loc[ df.label == 306940, ["mA", "mH"]  ] = 250,130
df.loc[ df.label == 306941, ["mA", "mH"]  ] = 230,150
df.loc[ df.label == 306942, ["mA", "mH"]  ] = 300,130
df.loc[ df.label == 306943, ["mA", "mH"]  ] = 300,150
df.loc[ df.label == 306944, ["mA", "mH"]  ] = 300,200
df.loc[ df.label == 306945, ["mA", "mH"]  ] = 350,250
df.loc[ df.label == 306946, ["mA", "mH"]  ] = 400,130
df.loc[ df.label == 306948, ["mA", "mH"]  ] = 400,200
df.loc[ df.label == 344587, ["mA", "mH"]  ] = 400,250
df.loc[ df.label == 306952, ["mA", "mH"]  ] = 500,130
df.loc[ df.label == 306955, ["mA", "mH"]  ] = 500,200
df.loc[ df.label == 306958, ["mA", "mH"]  ] = 500,300
df.loc[ df.label == 308468, ["mA", "mH"]  ] = 500,350
df.loc[ df.label == 306959, ["mA", "mH"]  ] = 500,400
df.loc[ df.label == 306962, ["mA", "mH"]  ] = 600,130
df.loc[ df.label == 306966, ["mA", "mH"]  ] = 600,300
df.loc[ df.label == 344588, ["mA", "mH"]  ] = 600,400
df.loc[ df.label == 308469, ["mA", "mH"]  ] = 600,450
df.loc[ df.label == 306967, ["mA", "mH"]  ] = 600,500
df.loc[ df.label == 306968, ["mA", "mH"]  ] = 700,130
df.loc[ df.label == 306970, ["mA", "mH"]  ] = 700,200
df.loc[ df.label == 306972, ["mA", "mH"]  ] = 700,300
df.loc[ df.label == 306973, ["mA", "mH"]  ] = 700,400
df.loc[ df.label == 306974, ["mA", "mH"]  ] = 700,500
df.loc[ df.label == 308568, ["mA", "mH"]  ] = 700,600
df.loc[ df.label == 308569, ["mA", "mH"]  ] = 800,130
df.loc[ df.label == 308570, ["mA", "mH"]  ] = 800,300
df.loc[ df.label == 344589, ["mA", "mH"]  ] = 800,500
df.loc[ df.label == 308571, ["mA", "mH"]  ] = 800,700

# 4. label signal events
logger.info('4. Label signal events: IsSignal')
df["IsSignal"] = False
df.loc[ df.label == 306939, "IsSignal"  ] = True
df.loc[ df.label == 306940, "IsSignal"  ] = True
df.loc[ df.label == 306941, "IsSignal"  ] = True
df.loc[ df.label == 306942, "IsSignal"  ] = True
df.loc[ df.label == 306943, "IsSignal"  ] = True
df.loc[ df.label == 306944, "IsSignal"  ] = True
df.loc[ df.label == 306945, "IsSignal"  ] = True
df.loc[ df.label == 306946, "IsSignal"  ] = True
df.loc[ df.label == 306948, "IsSignal"  ] = True
df.loc[ df.label == 344587, "IsSignal"  ] = True
df.loc[ df.label == 306952, "IsSignal"  ] = True
df.loc[ df.label == 306955, "IsSignal"  ] = True
df.loc[ df.label == 306958, "IsSignal"  ] = True
df.loc[ df.label == 308468, "IsSignal"  ] = True
df.loc[ df.label == 306959, "IsSignal"  ] = True
df.loc[ df.label == 306962, "IsSignal"  ] = True
df.loc[ df.label == 306966, "IsSignal"  ] = True
df.loc[ df.label == 344588, "IsSignal"  ] = True
df.loc[ df.label == 308469, "IsSignal"  ] = True
df.loc[ df.label == 306967, "IsSignal"  ] = True
df.loc[ df.label == 306968, "IsSignal"  ] = True
df.loc[ df.label == 306970, "IsSignal"  ] = True
df.loc[ df.label == 306972, "IsSignal"  ] = True
df.loc[ df.label == 306973, "IsSignal"  ] = True
df.loc[ df.label == 306974, "IsSignal"  ] = True
df.loc[ df.label == 308568, "IsSignal"  ] = True
df.loc[ df.label == 308569, "IsSignal"  ] = True
df.loc[ df.label == 308570, "IsSignal"  ] = True
df.loc[ df.label == 344589, "IsSignal"  ] = True
df.loc[ df.label == 308571, "IsSignal"  ] = True
nsig = np.count_nonzero( df['IsSignal'] )
nbkg = len(df)-nsig
logger.info('#evt of signal with all mA,mH: {}'.format(nsig) )
logger.info('#evt of all bkg: {}'.format(nbkg) )


logger.info('Print df summary before save:')
#print(df.describe(include='all'))
print(df)
logger.info('Now to disk using pickle with protocol 3...') # v3 explicit support for bytes objects
with open( sys.argv[1], 'wb') as savehandle:
    pickle.dump(df, savehandle, protocol=3)
logger.info('Done in {}'.format(sys.argv[1]))

