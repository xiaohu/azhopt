import numpy as np

from sklearn.utils import shuffle
from sklearn.preprocessing import QuantileTransformer, MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score, roc_curve, auc
from sklearn.externals import joblib

from keras.models import Model, load_model
from keras.layers import Input, Dense, Dropout
from keras.regularizers import l1_l2
from keras.optimizers import SGD
from keras import backend as K
from keras.utils import plot_model


#############################################################
def get_model_nn(n_invars, n_hidden_nodes, regularization=None, dropout=None):
    if not isinstance(n_hidden_nodes, list):
        n_hidden_nodes = [n_hidden_nodes]
        
    x = Input(shape=(n_invars,))
    d = x
    for n in n_hidden_nodes:
        d = Dense(n, activation="relu", kernel_regularizer=regularization)(d)
        if dropout:
            d = Dropout(dropout)(d)
    y = Dense(1, activation="sigmoid")(d)
    return Model(x, y)

# tranform variables to finite ranges
# only works for arr[x,y,z,mA] with arr[-1] being the param
##########################################################
def preprocess(X, quantile_trsf=None, minmax_trsf=None):
    if quantile_trsf is None and minmax_trsf is None:
        quantile_trsf = QuantileTransformer()
        minmax_trsf = MinMaxScaler()
        quantile_trsf.fit(X[:, :-1])
        minmax_trsf.fit(X[:, -1][:, np.newaxis])
    elif quantile_trsf is not None and minmax_trsf is not None:
        pass
    else:
        return None
    X_trsf_no_mass = quantile_trsf.transform(X[:, :-1])
    X_trsf_mass = minmax_trsf.transform(X[:, -1][:, np.newaxis])

    return quantile_trsf, minmax_trsf, np.hstack([X_trsf_no_mass, X_trsf_mass])

# tranform variables to finite ranges
# var: features to transform, e.g. pt eta phi etc.
# param: truth value param to transform, e.g. mA, mH
# notrfs: varaibles that does not need transform, e.g. IsSignal
##########################################################
def transform( var, quantile_trsf, param, minmax_trsf, notrfs ):
    if quantile_trsf is None and minmax_trsf is None:
        quantile_trsf = QuantileTransformer()
        minmax_trsf = MinMaxScaler()
        quantile_trsf.fit(var)
        minmax_trsf.fit(param)
    elif quantile_trsf is not None and minmax_trsf is not None:
        pass
    else:
        return None
    var_trsf = quantile_trsf.transform(var)
    param_trsf = minmax_trsf.transform(param)
    
    if notrfs is None:
        return quantile_trsf, minmax_trsf, np.hstack([var_trsf, param_trsf])
    else:
        return quantile_trsf, minmax_trsf, np.hstack([var_trsf, param_trsf, notrfs])


##########################################################
# Y: 0,1 bkg,sig
# predict: sigmoid output [0,1]
def bkg_rej_vs_sig_eff( Y, predict, weight=None ):
    #_l1 = np.linspace(0,0.2,40, endpoint=False)
    #_l2 = np.linspace(0.2,0.8,60, endpoint=False)
    #_l3 = np.linspace(0.8,1.0,40+1, endpoint=True)
    #_grid = np.concatenate( (_l1,_l2,_l3), axis=0 )
    _grid = np.linspace( 0, 1, 50+1, endpoint=True)

    if weight is None:
      weight = np.ones_like(Y)

    _tot_sig = float((Y * weight ).sum())
    _tot_bkg = float(( (1-Y) * weight ).sum())

    sig_eff = []
    bkg_rej = []

    for _cut in _grid:
        _sel = predict > _cut
        _sig = (Y[_sel] * weight[_sel] ).sum()
        _bkg = ( (1-Y[_sel]) * weight[_sel] ).sum()

        sig_eff.append( _sig / _tot_sig )
        bkg_rej.append( 1 - _bkg / _tot_bkg )

    return [ sig_eff, bkg_rej ]



