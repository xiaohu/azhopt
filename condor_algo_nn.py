import os

# espresso     = 20 minutes
# microcentury = 1 hour
# longlunch    = 2 hours
# workday      = 8 hours
# tomorrow     = 1 day
# testmatch    = 3 days
# nextweek     = 1 week
# 1 CPU ~ 2G ram

# INPUTs
# config in e.g. config_algo_nn.py
config_list = ['test_first','test_second','test_third']
# # #
root_folder = os.getcwd() # where all py scripts sit
condor_folder = '__condor__'
channel = "algo_nn" # prefix of dir per job {root_folder}/{condor_folder}/{channel_0021}/
nJob = len(config_list)
ncpu = 8
flavor = 'workday'
firstjob = 0 # new production
retrylist = [] # if non-empty, only send ijob in the list

def submitJob( ijob, jobfolder ): # jobfolder=jobDescription+#job

    # job config
    bsubfdir = root_folder+"/"+condor_folder+"/"+jobfolder+"/"
    bsubfname = bsubfdir+"/send_%05d.sh" % ijob
    bsubFile = open( bsubfname, "w" )
    text = getJobDef( ijob, jobfolder )
    bsubFile.write(text)
    bsubFile.close()

    # job submittion
    os.system( 'chmod -R 775 ' +bsubfdir )
    os.system( 'chmod +x '+bsubfname )
    cmd = "condor_submit"
    config = '''
executable            = {0}
arguments             = {1}
output                = {2}/stdout_{3:05d}.txt
error                 = {2}/stdout_{3:05d}.txt
log                   = {2}/condor_{3:05d}.txt
request_cpus          = {4}
+JobFlavour           = "{5}"
queue
    '''.format( bsubfname, '', root_folder+"/"+condor_folder+"/", ijob, ncpu, flavor )

    file_sub = '{0}/condor_submit'.format( bsubfdir )
    with open( file_sub , 'w') as _file_sub:
        _file_sub.write( config )

    cmd = cmd + ' < ' + file_sub
    print(cmd)
    os.system( 'cat {0}'.format(file_sub) )
    print('Condor does not support EOS for log and err. Use AFS.')
    os.system(cmd)

def getJobDef( ijob, jobfolder ):
    text = ''
    if channel == 'algo_nn':
      text = getJobDef_algo_nn( ijob, jobfolder)
    return text

def getJobDef_algo_nn( ijob, jobfolder ):

    text = """#!/bin/bash

uname -a

cat /proc/cpuinfo

cd {0}/{1}/{2} 2> /dev/null || {{ echo "The directory does not exist."; exit -1; }}
echo Current folder is
pwd
ls -l

echo "install setupatlas"
export AtlasSetup=/cvmfs/atlas.cern.ch/repo/sw/AtlasSetup/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

source {0}/setup.sh
ulimit -S -s 20000

cp {0}/*.py ./

python algo_nn.py {4}
""".format( root_folder, # 0
            condor_folder, # 1
            jobfolder, # 2
            channel, # 3
            config_list[ijob], # 4
          )

    return text


### main ###


for ijob in range(nJob):

  ijob += firstjob

  jobfolder = ("{0}_{1:04d}_{2}".format(channel, ijob, config_list[ijob]))

  # launch all
  if len(retrylist) == 0:
    print("Prepare workdir %s" % jobfolder)
    os.system("mkdir -vp %s/%s" % ( condor_folder, jobfolder) )
    submitJob( ijob, jobfolder )

  # launch failed from last runs
  else:
    if ijob in retrylist:
      print("Launch failed from last runs: ijob %d ..." % ijob)
      print("Clean workdir %s" % jobfolder)
      os.system("rm -rf %s/%s/*" % ( condor_folder, jobfolder) )
      submitJob( ijob, jobfolder )


