# Not in use now, as I found /eos/user is more than 10x faster than /eos/atlas/user in terms of IO !!!!!
# 
# split input single npy file into many, later processes will be performed on each in local cluster

import numpy as np
import os
from xutil import *

sure_to_call('splitit')

the_input = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy'
nevt_per_file = 1000*200

#
logger = setup_custom_logger('split_input')

#
the_output_nm = os.path.splitext(os.path.basename(the_input))[0]
the_output = '{0}/{1}.split'.format(os.path.dirname(the_input), the_output_nm)
logger.info('Create dir to store splitted files: {0}'.format(the_output))
if os.path.isdir(the_output):
    logger.error('Output dir exist: {0}  EXIT'.format(the_output))
    exit(1)
else:
    os.mkdir(the_output)

#
logger.info('Load in np array from npy: {0}'.format(the_input))
original_array = np.load(the_input)
logger.info('Size of the array: {0}'.format(original_array.shape))

#
logger.info('Split and write to disk ...')
nevt_total = original_array.shape[0]
for ith,idx in enumerate( range(0, nevt_total, nevt_per_file) ):
    _output = '{0}/{1}._{2:04d}.npy'.format(the_output, the_output_nm, ith)
    _idx_till = idx + nevt_per_file
    if( _idx_till > nevt_total ):
        _idx_till = nevt_total
    piece_array = original_array[idx:_idx_till]
    np.save(_output, piece_array)
    logger.info('Write {0}:{1} to file {2}'.format(idx,_idx_till,_output))

#
logger.info('All done')

