import numpy as np

# map mA,mH -> DISD
dict_mAmH_dsid = {
  'ggF_llbb' : {
    (230,130) : 306939, 
    (250,130) : 306940,
    (230,150) : 306941,
    (300,130) : 306942,
    (300,150) : 306943,
    (300,200) : 306944,
    (350,250) : 306945,
    (400,130) : 306946,
    (400,200) : 306948,
    (400,250) : 344587,
    (500,130) : 306952,
    (500,200) : 306955,
    (500,300) : 306958,
    (500,350) : 308468,
    (500,400) : 306959,
    (600,130) : 306962,
    (600,300) : 306966,
    (600,400) : 344588,
    (600,450) : 308469,
    (600,500) : 306967,
    (700,130) : 306968,
    (700,200) : 306970,
    (700,300) : 306972,
    (700,400) : 306973,
    (700,500) : 306974,
    (700,600) : 308568,
    (800,130) : 308569,
    (800,300) : 308570,
    (800,500) : 344589,
    (800,700) : 308571,
  },
}

mH_min = 130
mH_max = 700
mA_min = 230
mA_max = 800

# DSID -> process
dict_dsid_process = {

  # ggF llbb signal
  (306939,) : 'ggFllbbmA230mH130',
  (306940,) : 'ggFllbbmA250mH130',
  (306941,) : 'ggFllbbmA250mH150',
  (306942,) : 'ggFllbbmA300mH130',
  (306943,) : 'ggFllbbmA300mH150',
  (306944,) : 'ggFllbbmA300mH200',
  (306945,) : 'ggFllbbmA350mH250',
  (306946,) : 'ggFllbbmA400mH130',
  (306948,) : 'ggFllbbmA400mH200',
  (306949,) : 'ggFllbbmA400mH300',
  (306952,) : 'ggFllbbmA500mH130',
  (306955,) : 'ggFllbbmA500mH200',
  (306958,) : 'ggFllbbmA500mH300',
  (306959,) : 'ggFllbbmA500mH400',
  (306962,) : 'ggFllbbmA600mH130',
  (306964,) : 'ggFllbbmA600mH200',
  (306966,) : 'ggFllbbmA600mH300',
  (306967,) : 'ggFllbbmA600mH500',
  (306968,) : 'ggFllbbmA700mH130',
  (306970,) : 'ggFllbbmA700mH200',
  (306972,) : 'ggFllbbmA700mH300',
  (306973,) : 'ggFllbbmA700mH400',
  (306974,) : 'ggFllbbmA700mH500',
  (308468,) : 'ggFllbbmA500mH350',
  (308469,) : 'ggFllbbmA600mH450',
  (308568,) : 'ggFllbbmA700mH600',
  (308569,) : 'ggFllbbmA800mH130',
  (308570,) : 'ggFllbbmA800mH300',
  (308571,) : 'ggFllbbmA800mH700',
  (344587,) : 'ggFllbbmA400mH250',
  (344588,) : 'ggFllbbmA600mH400',
  (344589,) : 'ggFllbbmA800mH500',
  (309743,) : 'ggFllbbmA300mH180',
  (309744,) : 'ggFllbbmA400mH280',
  (309745,) : 'ggFllbbmA450mH350',
  (309746,) : 'ggFllbbmA500mH380',
  (309747,) : 'ggFllbbmA550mH450',
  (309748,) : 'ggFllbbmA600mH480',
  (309749,) : 'ggFllbbmA650mH550',
  (309750,) : 'ggFllbbmA700mH550',
  (309751,) : 'ggFllbbmA700mH580',
  (309752,) : 'ggFllbbmA750mH650',
  (309753,) : 'ggFllbbmA800mH200',
  (309754,) : 'ggFllbbmA800mH400',
  (309755,) : 'ggFllbbmA800mH600',
  (309756,) : 'ggFllbbmA800mH650',
  (309757,) : 'ggFllbbmA800mH680',
  (309757,) : 'ggFllbbmA800mH680',

  # ggF llbb signal high mass
  (310179,) : 'ggFllbbmA1000mH200',
  (310180,) : 'ggFllbbmA1000mH500',
  (310181,) : 'ggFllbbmA1000mH800',
  (310182,) : 'ggFllbbmA1200mH200',
  (310183,) : 'ggFllbbmA1200mH500',
  (310184,) : 'ggFllbbmA1200mH1100',

  # ggF llbb signal LW
  (306975,) : 'ggFllbbmA500mH150LW05',
  (306976,) : 'ggFllbbmA500mH150LW05',
  (306977,) : 'ggFllbbmA500mH150LW05',
  (306978,) : 'ggFllbbmA600mH300LW10',
  (306979,) : 'ggFllbbmA600mH300LW10',
  (306980,) : 'ggFllbbmA600mH300LW10',
  (306981,) : 'ggFllbbmA700mH400LW20',
  (306982,) : 'ggFllbbmA700mH400LW20',
  (306983,) : 'ggFllbbmA700mH400LW20',

  # bbA llbb signal
  (307513,) : 'bbAllbbmA800mH500',
  (308496,) : 'bbAllbbmA230mH130',
  (308509,) : 'bbAllbbmA500mH400',
  (308513,) : 'bbAllbbmA600mH300',
  (308515,) : 'bbAllbbmA600mH500',
  (308517,) : 'bbAllbbmA700mH200',
  (308520,) : 'bbAllbbmA700mH400',
  (308527,) : 'bbAllbbmA800mH700',

  # backgrounds, using internal id (NOT DSID)

  (0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
   17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
   34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45) : 'Zjets',
  (46,47) : 'ttbar',
  (48,49,50) : 'single top',
  (51,52,53,54,55,56) : 'ttV',
  (57,58,59) : 'VV',
  (60,61,62) : 'ZH',
  (63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80) : 'Wjets',

}

# process -> DISD
dict_process_disd = {

  # ggF llbb signal
  'ggFllbbmA230mH130' : [306939],
  'ggFllbbmA250mH130' : [306940],
  'ggFllbbmA250mH150' : [306941],
  'ggFllbbmA300mH130' : [306942],
  'ggFllbbmA300mH150' : [306943],
  'ggFllbbmA300mH200' : [306944],
  'ggFllbbmA350mH250' : [306945],
  'ggFllbbmA400mH130' : [306946],
  'ggFllbbmA400mH200' : [306948],
  'ggFllbbmA400mH300' : [306949],
  'ggFllbbmA500mH130' : [306952],
  'ggFllbbmA500mH200' : [306955],
  'ggFllbbmA500mH300' : [306958],
  'ggFllbbmA500mH400' : [306959],
  'ggFllbbmA600mH130' : [306962],
  'ggFllbbmA600mH200' : [306964],
  'ggFllbbmA600mH300' : [306966],
  'ggFllbbmA600mH500' : [306967],
  'ggFllbbmA700mH130' : [306968],
  'ggFllbbmA700mH200' : [306970],
  'ggFllbbmA700mH300' : [306972],
  'ggFllbbmA700mH400' : [306973],
  'ggFllbbmA700mH500' : [306974],
  'ggFllbbmA500mH350' : [308468],
  'ggFllbbmA600mH450' : [308469],
  'ggFllbbmA700mH600' : [308568],
  'ggFllbbmA800mH130' : [308569],
  'ggFllbbmA800mH300' : [308570],
  'ggFllbbmA800mH700' : [308571],
  'ggFllbbmA400mH250' : [344587],
  'ggFllbbmA600mH400' : [344588],
  'ggFllbbmA800mH500' : [344589],
  'ggFllbbmA300mH180' : [309743],
  'ggFllbbmA400mH280' : [309744],
  'ggFllbbmA450mH350' : [309745],
  'ggFllbbmA500mH380' : [309746],
  'ggFllbbmA550mH450' : [309747],
  'ggFllbbmA600mH480' : [309748],
  'ggFllbbmA650mH550' : [309749],
  'ggFllbbmA700mH550' : [309750],
  'ggFllbbmA700mH580' : [309751],
  'ggFllbbmA750mH650' : [309752],
  'ggFllbbmA800mH200' : [309753],
  'ggFllbbmA800mH400' : [309754],
  'ggFllbbmA800mH600' : [309755],
  'ggFllbbmA800mH650' : [309756],
  'ggFllbbmA800mH680' : [309757],
  'ggFllbbmA800mH680' : [309757],

  # ggF llbb signal high mass
  'ggFllbbmA1000mH200' : [310179],
  'ggFllbbmA1000mH500' : [310180],
  'ggFllbbmA1000mH800' : [310181],
  'ggFllbbmA1200mH200' : [310182],
  'ggFllbbmA1200mH500' : [310183],
  'ggFllbbmA1200mH1100': [310184],

  # ggF llbb signal LW
  'ggFllbbmA500mH150LW05' : [306975],
  'ggFllbbmA500mH150LW05' : [306976],
  'ggFllbbmA500mH150LW05' : [306977],
  'ggFllbbmA600mH300LW10' : [306978],
  'ggFllbbmA600mH300LW10' : [306979],
  'ggFllbbmA600mH300LW10' : [306980],
  'ggFllbbmA700mH400LW20' : [306981],
  'ggFllbbmA700mH400LW20' : [306982],
  'ggFllbbmA700mH400LW20' : [306983],

  # bbA llbb signal
  'bbAllbbmA800mH500' : [307513],
  'bbAllbbmA230mH130' : [308496],
  'bbAllbbmA500mH400' : [308509],
  'bbAllbbmA600mH300' : [308513],
  'bbAllbbmA600mH500' : [308515],
  'bbAllbbmA700mH200' : [308517],
  'bbAllbbmA700mH400' : [308520],
  'bbAllbbmA800mH700' : [308527],

  # backgrounds, using internal id (NOT DSID)

  'Zjets' : [0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
             17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
             34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45],
  'ttbar' : [46,47],
  'single top' : [48,49,50],
  'ttV' : [51,52,53,54,55,56],
  'VV' : [57,58,59],
  'ZH' : [60,61,62],
  'Wjets' : [63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80],

}

# full list of ggF llbb NW mass points <=800GeV
ggFllbb_allsample = [
  'ggFllbbmA230mH130',
  'ggFllbbmA250mH130',
  'ggFllbbmA250mH150',
  'ggFllbbmA300mH130',
  'ggFllbbmA300mH150',
  'ggFllbbmA300mH200',
  'ggFllbbmA350mH250',
  'ggFllbbmA400mH130',
  'ggFllbbmA400mH200',
  'ggFllbbmA400mH300',
  'ggFllbbmA500mH130',
  'ggFllbbmA500mH200',
  'ggFllbbmA500mH300',
  'ggFllbbmA500mH400',
  'ggFllbbmA600mH130',
  'ggFllbbmA600mH200',
  'ggFllbbmA600mH300',
  'ggFllbbmA600mH500',
  'ggFllbbmA700mH130',
  'ggFllbbmA700mH200',
  'ggFllbbmA700mH300',
  'ggFllbbmA700mH400',
  'ggFllbbmA700mH500',
  'ggFllbbmA500mH350',
  'ggFllbbmA600mH450',
  'ggFllbbmA700mH600',
  'ggFllbbmA800mH130',
  'ggFllbbmA800mH300',
  'ggFllbbmA800mH700',
  'ggFllbbmA400mH250',
  'ggFllbbmA600mH400',
  'ggFllbbmA800mH500',
  'ggFllbbmA300mH180',
  'ggFllbbmA400mH280',
  'ggFllbbmA450mH350',
  'ggFllbbmA500mH380',
  'ggFllbbmA550mH450',
  'ggFllbbmA600mH480',
  'ggFllbbmA650mH550',
  'ggFllbbmA700mH550',
  'ggFllbbmA700mH580',
  'ggFllbbmA750mH650',
  'ggFllbbmA800mH200',
  'ggFllbbmA800mH400',
  'ggFllbbmA800mH600',
  'ggFllbbmA800mH650',
  'ggFllbbmA800mH680',
  'ggFllbbmA800mH680',
]

# label (DSID or internal ID form Liverpool) -> process
#############################################################
# input [1]: label
# output [1]: process name
def _label2process( label ):
  for _k, _c in dict_dsid_process.items():
    if label in _k:
      return _c
  return 'NonProcess'
# return 1 array
def label2process( label ):
  _the_ufunc = np.frompyfunc( _label2process, 1, 1 )
  return _the_ufunc( label )

# process -> dsid or id
#############################################################
# input [1]: process
# output [1]: list of dsid or id
def process2labelarray( list_process ):
  list_id = []
  for process in list_process:
    list_id = list_id + dict_process_disd[process]
  return list_id

# mAmH -> DSID list
#############################################################
# mAmH_list is a list of mA,mH pair
# prod: ggF_llbb, bbA_llbb ...
def mAmH2labelarray( mAmH_list, prod ):
  return [ dict_mAmH_dsid[prod][(mA,mH)] for mA,mH in mAmH_list ]

# random sampling of background mA and mH
#############################################################
# nm: mA or mH
# the whole df
def sample_bkg_mass(nm, df, method=0, seed=None):
  n_bkg = (df.IsSignal == False).sum()
  m_bkg = None
  if seed:
    np.random.seed(seed)
  if method==0:
    m_bkg = np.random.choice(df[nm][df.IsSignal],size=n_bkg) # with replacement, generate bkg from sig distribution
  else:
    #m_bkg = np.random.uniform(np.min(m[isSig]),np.max(m[isSig]),size=n_bkg)
    if nm == 'mA':
      m_bkg = np.random.uniform( mA_min, mH_min, size=n_bkg)
    if nm == 'mH':
      m_bkg = np.random.uniform( mH_min, mA_max, size=n_bkg)
  df.loc[df.IsSignal==False, nm] = m_bkg

# choose signal samples
#############################################################
# mAmH_list is a list of mA,mH pair
# prod: ggF_llbb, bbA_llbb ...
def choose_signal_samples( df, list_process ):
  print('choose_signal_samples: signal tot # =',df.IsSignal.sum())
  dsid_list = process2labelarray( list_process )
  df_chosen = df[ ((df.IsSignal) & (df.label.isin(dsid_list)) ) | (~df.IsSignal) ]
  print('choose_signal_samples: signal tot chosen to use # =',df_chosen.IsSignal.sum() )
  return df_chosen

# list input variables
#############################################################
# make sure consistent with variable_preprocess.py
def input_variables( do3tag, df ):
  inputs = None
  if do3tag:
    inputs = df[["lep0pt","lep0eta","lep0phi",
             "lep1pt","lep1eta","lep1phi",
             "jet0pt","jet0eta","jet0phi",
             "jet1pt","jet1eta","jet1phi",
             "jet2pt","jet2eta","jet2phi",
             "MET","METSig",
             "mA","mH"]].values
  else:
    inputs = df[["lep0pt","lep0eta","lep0phi",
             "lep1pt","lep1eta","lep1phi",
             "jet0pt","jet0eta","jet0phi",
             "jet1pt","jet1eta","jet1phi",
             "MET","METSig",
             "mA","mH"]].values
  return inputs

#############################################################
# numpy ufunc frompyfunc for the moment
# in future can use C function to improve performance
#############################################################

# variables from two-object system
#############################################################
# input [6]: pt eta phi of each objet
# return [8]: pt, eta, phi, E, m, deta, dphi, dR of 2-object system
# consider m=0 for objects
def _variable_of_2obj( pt0, eta0, phi0, pt1, eta1, phi1 ):
  import ROOT
  lvec0 = ROOT.TLorentzVector()
  lvec1 = ROOT.TLorentzVector()
  lvec0.SetPtEtaPhiM(pt0, eta0, phi0, 0)
  lvec1.SetPtEtaPhiM(pt1, eta1, phi1, 0)
  lvec = lvec0+lvec1
  return np.float64(lvec.Pt()), np.float64(lvec.Eta()), np.float64(lvec.Phi()), np.float64(lvec.E()), np.float64(lvec.M()), \
         np.float64(ROOT.TMath.Abs(lvec0.Eta()-lvec1.Eta())), np.float64(lvec0.DeltaPhi(lvec1)), np.float64(lvec0.DeltaR(lvec1))

# return 8 arrays in a tuple
def variable_of_2obj( pt0, eta0, phi0, pt1, eta1, phi1 ):
  _the_ufunc = np.frompyfunc( _variable_of_2obj, 6, 8 )
  return _the_ufunc( pt0, eta0, phi0, pt1, eta1, phi1 )

# variables from four-object system
#############################################################
# input [12]: pt eta phi of each objet
# return [8]: pt, eta, phi, E, m, deta, dphi, dR of 2-object system
# consider m=0 for objects
def _variable_of_4obj( pt00, eta00, phi00, pt01, eta01, phi01, pt10, eta10, phi10, pt11, eta11, phi11 ):
  import ROOT
  #
  lvec00 = ROOT.TLorentzVector()
  lvec01 = ROOT.TLorentzVector()
  lvec00.SetPtEtaPhiM(pt00, eta00, phi00, 0)
  lvec01.SetPtEtaPhiM(pt01, eta01, phi01, 0)
  lvec0 = lvec00+lvec01
  #
  lvec10 = ROOT.TLorentzVector()
  lvec11 = ROOT.TLorentzVector()
  lvec10.SetPtEtaPhiM(pt10, eta10, phi10, 0)
  lvec11.SetPtEtaPhiM(pt11, eta11, phi11, 0)
  lvec1 = lvec10+lvec11

  lvec = lvec0+lvec1
  return np.float64(lvec.Pt()), np.float64(lvec.Eta()), np.float64(lvec.Phi()), np.float64(lvec.E()), np.float64(lvec.M()), \
         np.float64(ROOT.TMath.Abs(lvec0.Eta()-lvec1.Eta())), np.float64(lvec0.DeltaPhi(lvec1)), np.float64(lvec0.DeltaR(lvec1))

# return 8 arrays in a tuple
def variable_of_4obj( pt00, eta00, phi00, pt01, eta01, phi01, pt10, eta10, phi10, pt11, eta11, phi11 ):
  _the_ufunc = np.frompyfunc( _variable_of_4obj, 12, 8 )
  return _the_ufunc( pt00, eta00, phi00, pt01, eta01, phi01, pt10, eta10, phi10, pt11, eta11, phi11 )

# jvar
#############################################################
# input [5]: pt of llbb, and mllbb
# return [1]: jvar
def _variable_of_jvar( ptl0, ptl1, ptj0, ptj1, mlljj ):
  return np.float64(np.sqrt(np.square(ptl0)+np.square(ptl1)+np.square(ptj0)+np.square(ptj1)) / mlljj)

# return 1 arrays in a tuple
def variable_of_jvar( ptl0, ptl1, ptj0, ptj1, mlljj ):
  _the_ufunc = np.frompyfunc( _variable_of_jvar, 5, 1 )
  return _the_ufunc( ptl0, ptl1, ptj0, ptj1, mlljj )

