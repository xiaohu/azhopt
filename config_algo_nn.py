#

test_2tag = {
             'thefile' : '/eos/user/x/xiaohu/AZH_EoR2/dataset/L20181001-2tag-lowlvl_trsf.npy',
             'doTraining' : True,
             'doSampleBkg': True,
             'do3tag' : False,
             'doTensorBoard' : False,
             'layer_size' : [100,100,100], #[200,200],
             'dropout' : None,
             'nepochs' : 50,
             'batch_size' : 64,
             }

test_3tag = {
             'thefile' : '/eos/user/x/xiaohu/AZH_EoR2/dataset/L20181001-3tag-lowlvl_trsf.npy',
             'doTraining' : True,
             'doSampleBkg': True,
             'do3tag' : True,
             'doTensorBoard' : False,
             'layer_size' : [200,200,200,200,200], #[200,200],
             'dropout' : None,
             'nepochs' : 50,
             'batch_size' : 64,
             }


test_test = {
             'thefile' : '/afs/cern.ch/work/x/xiaohu/AZH_EoR2/AZHOpt/dummy_lowlvl_trsf.npy',
             'doTraining' : True,
             'doSampleBkg': False,
             'do3tag' : False,
             'doTensorBoard' : False,
             'layer_size' : [20],
             'dropout' : None,
             'nepochs' : 50,
             'batch_size' : 128,
             }


