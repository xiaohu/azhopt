# merge splitted/processed inputs

import os
import pandas as pd
import pickle
from xutil import *

the_file = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.split/20181001_prefix-pNN_input_2tag_Sig_BKGs._{0:04d}.add_variable.pickle'
the_output = '20181001_prefix-pNN_input_2tag_Sig_BKGs.add_variable.pickle'
nfile = 85

#
logger = setup_custom_logger('merge_input')

#
logger.info('Merge dataframe from files in {0}'.format(os.path.dirname(the_file)))
df = None
for ifile in range(nfile):
  _filenm = the_file.format(ifile)
  logger.info('Merging {0}'.format(_filenm))
  with open( _filenm, 'rb' ) as _handle:
    _df = pickle.load(_handle)
  if ifile == 0:
    df = _df
  else:
    df = pd.concat( [df, _df] )
  logger.info('Concatenated nevt: {0}'.format(len(_df)))

#
logger.info('Finish merging, tot nevt: {0}'.format(len(df)))
logger.info('Saving to disk ...')
with open( the_output, 'wb' ) as _handle:
  pickle.dump(df, _handle, protocal=3)
logger.info('All done')
