# read in orignal liverpool inputs
# output large pickle with many kinematic variables

import os
import pickle
import numpy as np
import pandas as pd
from xutil import *
from phyutil import *

def drop_signal_add_variable(the_input, the_output, save_opt = 'pickle'):
  #
  logger = setup_custom_logger('drop_signal_add_variable')

  #
  logger.info('Loading ... {0}'.format(the_input))
  the_array = np.load(the_input)
  logger.info('Loaded np.array with a shape {0}'.format(the_array.shape))
  logger.info('Feed to df')
  df = pd.DataFrame(data=the_array, columns=["label","weight",
                                         "lep0pt","lep0eta","lep0phi",
                                         "lep1pt","lep1eta","lep1phi",
                                         "jet0pt","jet0eta","jet0phi",
                                         "jet1pt","jet1eta","jet1phi",
                                         "jet2pt","jet2eta","jet2phi",
                                         "MET","METSig","NBJet","NEvent"])
  # remove some col
  logger.info('Drop columns')
  df = df[ ["label","weight",
            "lep0pt","lep0eta","lep0phi","lep1pt","lep1eta","lep1phi",
            "jet0pt","jet0eta","jet0phi","jet1pt","jet1eta","jet1phi","jet2pt","jet2eta","jet2phi",
            "MET","METSig"] ]
  
  # NW and LW both are kept
  logger.info('Add columns mA,mH for signal excluding LW and high mass!')
  nframes = len(df)
  df['mA'] = np.zeros(nframes)
  df['mH'] = np.zeros(nframes)
  df['IsSignal'] = None
  # ggF llbb NW 48 samples
  df.loc[ df.label == 306939, ['mA', 'mH', 'IsSignal']  ] = 230,130,True
  df.loc[ df.label == 306940, ['mA', 'mH', 'IsSignal']  ] = 250,130,True
  df.loc[ df.label == 306941, ['mA', 'mH', 'IsSignal']  ] = 250,150,True
  df.loc[ df.label == 306942, ['mA', 'mH', 'IsSignal']  ] = 300,130,True
  df.loc[ df.label == 306943, ['mA', 'mH', 'IsSignal']  ] = 300,150,True
  df.loc[ df.label == 306944, ['mA', 'mH', 'IsSignal']  ] = 300,200,True
  df.loc[ df.label == 306945, ['mA', 'mH', 'IsSignal']  ] = 350,250,True
  df.loc[ df.label == 306946, ['mA', 'mH', 'IsSignal']  ] = 400,130,True
  df.loc[ df.label == 306948, ['mA', 'mH', 'IsSignal']  ] = 400,200,True
  df.loc[ df.label == 306949, ['mA', 'mH', 'IsSignal']  ] = 400,300,True
  df.loc[ df.label == 306952, ['mA', 'mH', 'IsSignal']  ] = 500,130,True
  df.loc[ df.label == 306955, ['mA', 'mH', 'IsSignal']  ] = 500,200,True
  df.loc[ df.label == 306958, ['mA', 'mH', 'IsSignal']  ] = 500,300,True
  df.loc[ df.label == 306959, ['mA', 'mH', 'IsSignal']  ] = 500,400,True
  df.loc[ df.label == 306962, ['mA', 'mH', 'IsSignal']  ] = 600,130,True
  df.loc[ df.label == 306964, ['mA', 'mH', 'IsSignal']  ] = 600,200,True
  df.loc[ df.label == 306966, ['mA', 'mH', 'IsSignal']  ] = 600,300,True
  df.loc[ df.label == 306967, ['mA', 'mH', 'IsSignal']  ] = 600,500,True
  df.loc[ df.label == 306968, ['mA', 'mH', 'IsSignal']  ] = 700,130,True
  df.loc[ df.label == 306970, ['mA', 'mH', 'IsSignal']  ] = 700,200,True
  df.loc[ df.label == 306972, ['mA', 'mH', 'IsSignal']  ] = 700,300,True
  df.loc[ df.label == 306973, ['mA', 'mH', 'IsSignal']  ] = 700,400,True
  df.loc[ df.label == 306974, ['mA', 'mH', 'IsSignal']  ] = 700,500,True
  df.loc[ df.label == 308468, ['mA', 'mH', 'IsSignal']  ] = 500,350,True
  df.loc[ df.label == 308469, ['mA', 'mH', 'IsSignal']  ] = 600,450,True
  df.loc[ df.label == 308568, ['mA', 'mH', 'IsSignal']  ] = 700,600,True
  df.loc[ df.label == 308569, ['mA', 'mH', 'IsSignal']  ] = 800,130,True
  df.loc[ df.label == 308570, ['mA', 'mH', 'IsSignal']  ] = 800,300,True
  df.loc[ df.label == 308571, ['mA', 'mH', 'IsSignal']  ] = 800,700,True
  df.loc[ df.label == 344587, ['mA', 'mH', 'IsSignal']  ] = 400,250,True
  df.loc[ df.label == 344588, ['mA', 'mH', 'IsSignal']  ] = 600,400,True
  df.loc[ df.label == 344589, ['mA', 'mH', 'IsSignal']  ] = 800,500,True
  df.loc[ df.label == 309743, ['mA', 'mH', 'IsSignal']  ] = 300,180,True
  df.loc[ df.label == 309744, ['mA', 'mH', 'IsSignal']  ] = 400,280,True
  df.loc[ df.label == 309745, ['mA', 'mH', 'IsSignal']  ] = 450,350,True
  df.loc[ df.label == 309746, ['mA', 'mH', 'IsSignal']  ] = 500,380,True
  df.loc[ df.label == 309747, ['mA', 'mH', 'IsSignal']  ] = 550,450,True
  df.loc[ df.label == 309748, ['mA', 'mH', 'IsSignal']  ] = 600,480,True
  df.loc[ df.label == 309749, ['mA', 'mH', 'IsSignal']  ] = 650,550,True
  df.loc[ df.label == 309750, ['mA', 'mH', 'IsSignal']  ] = 700,550,True
  df.loc[ df.label == 309751, ['mA', 'mH', 'IsSignal']  ] = 700,580,True
  df.loc[ df.label == 309752, ['mA', 'mH', 'IsSignal']  ] = 750,650,True
  df.loc[ df.label == 309753, ['mA', 'mH', 'IsSignal']  ] = 800,200,True
  df.loc[ df.label == 309754, ['mA', 'mH', 'IsSignal']  ] = 800,400,True
  df.loc[ df.label == 309755, ['mA', 'mH', 'IsSignal']  ] = 800,600,True
  df.loc[ df.label == 309756, ['mA', 'mH', 'IsSignal']  ] = 800,650,True
  df.loc[ df.label == 309757, ['mA', 'mH', 'IsSignal']  ] = 800,680,True
  df.loc[ df.label == 309757, ['mA', 'mH', 'IsSignal']  ] = 800,680,True
  # ggF llbb high mass
  #df.loc[ df.label == 310179, ['mA', 'mH', 'IsSignal']  ] = 1000,200,True
  #df.loc[ df.label == 310180, ['mA', 'mH', 'IsSignal']  ] = 1000,500,True
  #df.loc[ df.label == 310181, ['mA', 'mH', 'IsSignal']  ] = 1000,800,True
  #df.loc[ df.label == 310182, ['mA', 'mH', 'IsSignal']  ] = 1200,200,True
  #df.loc[ df.label == 310183, ['mA', 'mH', 'IsSignal']  ] = 1200,500,True
  #df.loc[ df.label == 310184, ['mA', 'mH', 'IsSignal']  ] = 1200,1100,True
  # ggF lbb LW LW LW LW LW LW LW LW
  #df.loc[ df.label == 306975, ['mA', 'mH', 'IsSignal']  ] = 500,150,True
  #df.loc[ df.label == 306976, ['mA', 'mH', 'IsSignal']  ] = 500,150,True
  #df.loc[ df.label == 306977, ['mA', 'mH', 'IsSignal']  ] = 500,150,True
  #df.loc[ df.label == 306978, ['mA', 'mH', 'IsSignal']  ] = 600,300,True
  #df.loc[ df.label == 306979, ['mA', 'mH', 'IsSignal']  ] = 600,300,True
  #df.loc[ df.label == 306980, ['mA', 'mH', 'IsSignal']  ] = 600,300,True
  #df.loc[ df.label == 306981, ['mA', 'mH', 'IsSignal']  ] = 700,400,True
  #df.loc[ df.label == 306982, ['mA', 'mH', 'IsSignal']  ] = 700,400,True
  #df.loc[ df.label == 306983, ['mA', 'mH', 'IsSignal']  ] = 700,400,True
  # bbA llbb
  #df.loc[ df.label == 307513, ['mA', 'mH', 'IsSignal']  ] = 800,500,True
  #df.loc[ df.label == 308496, ['mA', 'mH', 'IsSignal']  ] = 230,130,True
  #df.loc[ df.label == 308509, ['mA', 'mH', 'IsSignal']  ] = 500,400,True
  #df.loc[ df.label == 308513, ['mA', 'mH', 'IsSignal']  ] = 600,300,True
  #df.loc[ df.label == 308515, ['mA', 'mH', 'IsSignal']  ] = 600,500,True
  #df.loc[ df.label == 308517, ['mA', 'mH', 'IsSignal']  ] = 700,200,True
  #df.loc[ df.label == 308520, ['mA', 'mH', 'IsSignal']  ] = 700,400,True
  #df.loc[ df.label == 308527, ['mA', 'mH', 'IsSignal']  ] = 800,700,True

  # bkg
  df.loc[ df.label < 1000, ['mA', 'mH', 'IsSignal']  ] = 0,0,False

  # drop immediately signal points not in use
  logger.info('Drop signal points not for ggF llbb NW')
  drop1 = df.index[ df['IsSignal'].values == None ]
  df.drop( df.index[drop1], inplace=True )
  logger.info('Dropped {0} events'.format(len(drop1)))
  logger.info('Now sample shape: {0}'.format(df.shape))
  nframes = len(df) # update after dropping

  # Generate random mA mH for bkg AFTER DROPPING useless signal events!
  # KEEP same distribution of mA mH between signal and backgrounds
  # otherwise NN try to use it as feature!
  # this depends on mA mH range, check what samplesa are included for signals !!!
  logger.info('Generating background masses according to signals taken into account')
  sample_bkg_mass('mA', df, 0) # mH
  sample_bkg_mass('mH', df, 0) # mA

  #
  logger.info('Adding variables ...')
  #
  logger.info('Adding ll variables ...')
  _res = variable_of_2obj( df['lep0pt'].values, df['lep0eta'].values, df['lep0phi'].values, 
                           df['lep1pt'].values, df['lep1eta'].values, df['lep1phi'].values )
  df['llpt'], df['lleta'], df['llphi'], df['lle'], df['llm'], df['lldeta'], df['lldphi'], df['lldR'] = _res
  #
  logger.info('Add jj variables ...')
  _res = variable_of_2obj( df['jet0pt'].values, df['jet0eta'].values, df['jet0phi'].values, 
                           df['jet1pt'].values, df['jet1eta'].values, df['jet1phi'].values )
  df['jjpt'], df['jjeta'], df['jjphi'], df['jje'], df['jjm'], df['jjdeta'], df['jjdphi'], df['jjdR'] = _res
  #
  logger.info('Add lljj veriables ...')
  _res = variable_of_4obj( df['lep0pt'].values, df['lep0eta'].values, df['lep0phi'].values, 
                           df['lep1pt'].values, df['lep1eta'].values, df['lep1phi'].values,
                           df['jet0pt'].values, df['jet0eta'].values, df['jet0phi'].values, 
                           df['jet1pt'].values, df['jet1eta'].values, df['jet1phi'].values )
  df['lljjpt'], df['lljjeta'], df['lljjphi'], df['lljje'], df['lljjm'], df['lljjdeta'], df['lljjdphi'], df['lljjdR'] = _res

  # to have it float64, use self-defined ufunc
  logger.info('Add Jvar ...')
  #df['jvar'] = np.sqrt(np.square(df['lep0pt'])+np.square(df['lep1pt'])+np.square(df['jet0pt'])+np.square(df['jet1pt'])) / df['lljjm']
  df['jvar'] = variable_of_jvar( df['lep0pt'].values, df['lep1pt'].values, df['jet0pt'].values, df['jet0pt'].values, df['lljjm'].values )

  # size too large with each as string, use label directly in user level
  #df['process'] = label2process( df['label'].values )

  #
  logger.info('Add EventNumber (need to use real ones in future!)')
  df['EventNumber'] = np.arange(nframes, dtype=np.int32) # int64

  logger.info('Addition done')
  
  print(df.head())

  # type conversion
  logger.info('Type conversion: use float64 and int32/64 as much as possible when applicable')
  for col in df.columns:
    if col == 'label':
      df[col] = df[col].astype('int32')
    elif col == 'NBJet' or col == 'mA' or col == 'mH':
      df[col] = df[col].astype('int16')
    elif col == 'NEvent':
      df[col] = df[col].astype('int64') # int64
    elif col == 'EventNumber':
      pass # already made with int64
    elif col == 'IsSignal':
      df[col] = df[col].astype('bool_') # most of time bool is useful, only before training/testing convert to int
    else:
      df[col] = df[col].astype('float32') # float64
  
  # save to disk to different formats
  if save_opt == 'pickle':
    the_output = the_output+str(pickle.HIGHEST_PROTOCOL)
    logger.info('Save dataframe to binary file {0}'.format(the_output))
    with open( the_output, 'wb' ) as _handle:
      pickle.dump(df, _handle, protocol=pickle.HIGHEST_PROTOCOL)
  elif save_opt == 'pickle2':
    the_output = the_output+str(2)
    logger.info('Save dataframe to binary file {0} with protocol specified to 2'.format(the_output))
    with open( the_output, 'wb' ) as _handle:
      pickle.dump(df, _handle, protocol=2)
  elif save_opt == 'feather':
    logger.info('Save dataframe to binary file {0}'.format(the_output))
    df.reset_index(drop=True, inplace=True)
    df.to_feather(the_output)
  else:
    # all columns will be converted to one type, the largest one (e.g. all->float64, or all->object)
    logger.info('Save raw np.array to binary file {0}'.format(the_output))
    np.save(the_output, df.values)
    print(df.values.shape)
    print(df.columns.values)
  logger.info('All done')
  

if __name__ == '__main__':
  
  drop_signal_add_variable( sys.argv[1], sys.argv[2], sys.argv[3] )

