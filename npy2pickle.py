# convert npy to pickle binary v3

import numpy as np
import sys
import pickle

### CONFIG ###
# this file contains all samples
thefile = "/eos/atlas/user/x/xiaohu/public/AZH_Optimisation/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy"
tofile = '20181001_prefix-pNN_input_2tag_Sig_BKGs.pickle'
### CONFIG ###

# system
import xutil
logger = xutil.setup_custom_logger('logger_npy2pickle')

# load
logger.info('Loading ... {}'.format(thefile))
myarr = np.load(thefile)

# convert
logger.info('Converting to pickle with protocol 3...') # v3 explicit support for bytes objects
with open( tofile, 'wb') as savehandle:
    pickle.dump(myarr, savehandle, protocol=3)
logger.info('Done')

