# test IO

import numpy as np
import pickle
from xutil import *
logger = setup_custom_logger('wilde_test')

the_input = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy'
#the_input = '/eos/atlas/user/x/xiaohu/public/AZH_Optimisation/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy'
logger.info('Load in np array from npy: {0}'.format(the_input))
original_array = np.load(the_input)
logger.info('Size of the array: {0}'.format(original_array.shape))

#the_input = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.pickle'
#logger.info('Load in np array from pickle: {0}'.format(the_input))
#datafile = open( the_input, 'rb' )
#original_array = pickle.load( datafile )
#logger.info('Size of the array: {0}'.format(original_array.shape))

