import argparse
import os
import re
from glob import glob
import numpy as np
import pandas as pd
import sys
from time import time

import pickle
from xutil import *
from phyutil import *
from mvautil import *

from sklearn.utils import shuffle
from sklearn.preprocessing import QuantileTransformer, MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score, roc_curve, auc
from sklearn.externals import joblib

from keras.models import Model, load_model
from keras.layers import Input, Dense, Dropout
from keras.regularizers import l1_l2
from keras.optimizers import SGD
from keras import backend as K
from keras.utils import plot_model
from keras.callbacks import TensorBoard
import keras as ks

import tensorflow as tf

def algo_nn(thefile, doTraining, do3tag, doTensorBoard, layer_size, dropout, nepochs, batch_size):

    ### CONFIG ###
    #thefile = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.add_variable.pickle'
    #thefile = 'test.pickle'
    ### END ###
    
    #do3tag = False
    #doTensorBoard = False
    
    #layer_size = [50]
    #dropout = None
    #nepochs = 50
    #batch_size = 128
    ### CONFIG ###
    
    layer_size_str = 'n'.join( [ str(i) for i in layer_size] )
    modeltag = 'nn_l{0}e{1}b{2}'.format(layer_size_str,nepochs,batch_size)
    
    # output files
    netfile = "model_{0}.h5".format(modeltag)
    graphfile = "graph_{0}.png".format(modeltag)
    file_minmax_scaler = "minmax_scaler_{0}.pickle".format(modeltag)
    file_quantile_scaler = "quantile_scaler_{0}.pickle".format(modeltag)
    file_performance = 'perf_{0}.pickle'.format(modeltag)
    file_roc = 'roc_{0}.pickle'.format(modeltag)
    
    # system
    logger = setup_custom_logger('algo_nn')
    
    # load in data
    logger.info('1. Load in dataset ...')
    datafile = open( thefile, 'rb' )
    df = pickle.load( datafile )
    logger.info('load in done')
    print(df)
    
    # prepare training testing dataset
    logger.info('2. Prepare training testing datasets')
    logger.info(' + choose signal samples (may not all mA,mH)')
    #df = choose_signal_samples( df, [ 'ggFllbbmA400mH200', 'ggFllbbmA700mH200', 'ggFllbbmA700mH500' ] )
    df = choose_signal_samples( df, ggFllbb_allsample ) # all ggF llbb NW samples <=800GeV

    # move to add_variable, this depends on signal mass distribution, check what signal is fed
    #logger.info(' + generate random mA,mH for bkg')
    #sample_bkg_mass('mA', df, 0) # mH min/max mass is fixed in phyutil.py; MAKE SURE bkg sig same distribution !!!!!
    #sample_bkg_mass('mH', df, 0) # mA

    logger.info(' + connect input variables depending on #bjets')
    X = input_variables( do3tag, df )
    logger.info(' + prepare target Y with type int')
    Y = df.IsSignal.values.astype(int) # int for cost function
    W = df.weight
    logger.info(' + shuffle all chosen entries')
    X, Y = shuffle(X,Y)
    logger.info(' + split sample for training 2/3, testing 1/3')
    X_train = np.concatenate( (X[::3], X[1::3]), axis=0 )
    Y_train = np.concatenate( (Y[::3], Y[1::3]), axis=0 )
    W_train = np.concatenate( (W[::3], W[1::3]), axis=0 )
    X_test = X[2::3]
    Y_test = Y[2::3]
    W_test = W[2::3]
    logger.info('Training sample: {0}; Tesiting sample: {1}'.format( len(X_train), len(X_test) ))
    
    # train and test NN
    model = None
    minmax_trsf = None
    quantile_trsf = None
    
    if doTraining:
        # training
        logger.info('3. Train NN with {0} layers, {1} epochs, {2} batch size, {3} dropout. model tag is {4}'.format(layer_size_str,nepochs,batch_size,dropout,modeltag) )
 
        _, n_invars = X.shape
        model = get_model_nn(n_invars, layer_size, dropout=dropout)
        model.compile(optimizer='sgd',loss="binary_crossentropy",metrics=["accuracy"])
        #plot_model(model, to_file=graphfile, show_shapes=True)
        # Preprocessing
        logger.info(' + preprocessing ...')
        quantile_trsf, minmax_trsf, X_train_trsf = preprocess(X_train)
        _,_, X_test_trsf = preprocess(X_test, quantile_trsf, minmax_trsf)
        
        logger.info(' + training ...')
        logger.info("{:>10}{:>10}".format("Epoch", "Loss"))
        tb = TensorBoard(log_dir="logs/{0}_{1}".format(modeltag,time()), batch_size=batch_size, histogram_freq=1, write_graph=False, write_images=False, write_grads=False) if doTensorBoard else None
        
        _res = model.fit(X_train_trsf, Y_train, batch_size=batch_size, epochs=nepochs, validation_data=(X_test_trsf,Y_test), callbacks=[tb], verbose=2) if doTensorBoard else model.fit(X_train_trsf, Y_train, batch_size=batch_size, epochs=nepochs, validation_data=(X_test_trsf,Y_test), verbose=2)
        
        # now save the network
        logger.info(' + dump model, transformers and history to files {0} {1} {2} {3}'.format(netfile,file_minmax_scaler,file_quantile_scaler,file_performance))
        joblib.dump(minmax_trsf, file_minmax_scaler)
        joblib.dump(quantile_trsf, file_quantile_scaler)
        model.save(netfile)
        with open( file_performance, 'wb') as _handle:
            pickle.dump(_res.history, _handle, protocol=2) # back compatible with python2
    else:
        logger.info('3. No training, directly app and test')
        model = load_model( netfile )
        minmax_trsf = joblib.load( file_minmax_scaler )
        quantile_trsf = joblib.load( file_quantile_scaler ) 

    # testing
    logger.info('4. Testing')
    Y_train_predict = model.predict(X_train_trsf)
    Y_test_predict = model.predict(X_test_trsf)
    Y_train_predict = np.squeeze(Y_train_predict) # (n,1) -> (n,)
    Y_test_predict = np.squeeze(Y_test_predict)
    bkg_rejection_vs_sig_efficiency = {}
    bkg_rejection_vs_sig_efficiency['train'] = bkg_rej_vs_sig_eff( Y_train, Y_train_predict, W_train )
    bkg_rejection_vs_sig_efficiency['test'] = bkg_rej_vs_sig_eff( Y_test, Y_test_predict, W_test )
    logger.info(' + saving bkg_rej_vs_sig_eff to {0}'.format(file_roc))
    with open( file_roc, 'wb' ) as _handle:
        pickle.dump( bkg_rejection_vs_sig_efficiency, _handle, protocol=2) # back compatible with python2

    logger.info('All done')
    
    
if __name__ == '__main__':
  
  import config_algo_nn
  _config = getattr( config_algo_nn, sys.argv[1] )
  algo_nn( **_config )

