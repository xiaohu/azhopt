import pickle
import sys

from xutil import *
from phyutil import *
from mvautil import *

from keras.models import Model, load_model
from sklearn.externals import joblib

def app_nn( do3tag, modelfile, minmaxfile, quantilefile, file_data, file_output, saveall ):
  ### INPUT ###
  #do3tag = False
  #modeltag = 'l50e50b128'
  #netpath = './__condor__/algo_nn_0000_test_first/'
  #file_data = 'dummy.pickle'
  #file_output = 'test_app_nn.pickle'
  #saveall = False
  #
  
  # system
  logger = setup_custom_logger('app_nn')
  
  # load in data
  logger.info('1. Load in dataset ...')
  df = None
  with open( file_data, 'rb') as _handle:
    df = pickle.load(_handle)
  logger.info(' + load in done, shape is {0}'.format(df.shape))
  
  # this was done in drop_signal_add_variable
  # prepare mA mH
  #logger.info('2. Generate mA mH of bkg')
  #sample_bkg_mass('mA', df, 1) # mH min/max mass is fixed in phyutil.py
  #sample_bkg_mass('mH', df, 1) # mA

  logger.info(' + connect input variables depending on #bjets')
  X = input_variables( do3tag, df )
  
  # apply transfomers
  logger.info( "3. Retrieve and apply nn, transformers")
  logger.info(' + read in files {0} {1} {2}'.format(modelfile,minmaxfile,quantilefile))
  model = load_model(modelfile)
  quantile_scaler = joblib.load(quantilefile)
  minmax_scaler = joblib.load(minmaxfile)
  logger.info('+ applying transformers ...')
  _, _, X_trsf = transform(X[:,:-2], quantile_scaler, X[:,-2:], minmax_scaler, None )

  logger.info(' + applying nn ...')
  target_nn = model.predict(X_trsf) # apply to all rows !
  df['nn'] = target_nn.astype('float64')
  
  # save to disk
  logger.info('4. Save df to binary file {0}'.format(file_output))
  if saveall :
    # save not only nn but also existing variables
    pass
  else:
    df = df[['label','EventNumber','nn','IsSignal','weight']]
  with open( file_output, 'wb' ) as _handle:
    pickle.dump(df, _handle, protocol=4) # for large object dumping
  
  logger.info('ALL DONE')

if __name__ == '__main__':

  if len(sys.argv) != 8:
    print('e.g. python app_nn 2tag ./model.h5 ./minmaxtf.pickle ./quantiletf.pickle input.pickle output_nn.pickle savesingle')
    exit(1)

  do3tag = False if sys.argv[1] != 'do3tag' else True
  modelfile = sys.argv[2]
  minmaxfile = sys.argv[3]
  quantilefile = sys.argv[4]
  file_data = sys.argv[5]
  file_output = sys.argv[6]
  saveall = False if sys.argv[7] != 'saveall' else True

  app_nn( do3tag, modelfile, minmaxfile, quantilefile, file_data, file_output, saveall )

