import argparse
import os
import re
from glob import glob
import numpy as np
import pandas as pd
import sys
from time import time

import pickle
from xutil import *
from phyutil import *
from mvautil import *
from datagenerator import DataGenerator

from sklearn.utils import shuffle
#from sklearn.preprocessing import QuantileTransformer, MinMaxScaler
#from sklearn.model_selection import StratifiedKFold
#from sklearn.metrics import roc_auc_score, roc_curve, auc
#from sklearn.externals import joblib

from keras.models import Model, load_model
from keras.layers import Input, Dense, Dropout
from keras.regularizers import l1_l2
from keras.optimizers import SGD
from keras import backend as K
from keras.utils import plot_model
from keras.callbacks import TensorBoard
import keras as ks
import tensorflow as tf

# avoid backending Xwindows
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def algo_nn(thefile, doTraining, doSampleBkg, do3tag, doTensorBoard, layer_size, dropout, nepochs, batch_size):

    ### CONFIG ###
    #thefile = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.add_variable.pickle'
    #thefile = 'test.pickle'
    ### END ###
    
    #do3tag = False
    #doTensorBoard = False
    
    #layer_size = [50]
    #dropout = None
    #nepochs = 50
    #batch_size = 128
    ### CONFIG ###
    
    layer_size_str = 'n'.join( [ str(i) for i in layer_size] )
    modeltag = 'nn_{0}_l{1}e{2}b{3}'.format('3tag' if do3tag else '2tag',layer_size_str,nepochs,batch_size)
    
    # output files
    netfile = "model_{0}.h5".format(modeltag)
    graphfile = "graph_{0}.png".format(modeltag)
    file_performance = 'perf_{0}.pickle'.format(modeltag)
    file_roc = 'roc_{0}.pickle'.format(modeltag)
    
    # system
    logger = setup_custom_logger('algo_nn')

    logger.info('NOTE read in all features afer transformation!')
    logger.info('Model tag {0}'.format(modeltag))

    # load in data
    logger.info('Load in dataset ...')
    arr = np.load( thefile )
    logger.info('load array {0}'.format(arr.shape))
    print(arr)
    
    logger.info('Get X and Y')
    X = arr[:,:-1] # -3: mA -2: mH
    Y = arr[:,-1].astype(int) # int for cost function
    #logger.info('Shuffle all chosen entries')
    #X, Y = shuffle(X,Y) # shuffle in datagenerator
    logger.info('Split sample for training 2/3 (8:2 train:val), testing 1/3')
    X_train_val = np.concatenate( (X[0::3,:], X[1::3,:]), axis=0 )
    Y_train_val = np.concatenate( (Y[0::3], Y[1::3]), axis=0 )
    X_train = np.concatenate( [X_train_val[i::10,:] for i in range(0,8)], axis=0 )
    Y_train = np.concatenate( [Y_train_val[i::10] for i in range(0,8)], axis=0 )
    X_val = np.concatenate( [X_train_val[i::10,:] for i in range(8,10)], axis=0 )
    Y_val = np.concatenate( [Y_train_val[i::10] for i in range(8,10)], axis=0 )
    X_test = X[2::3,:]
    Y_test = Y[2::3]
    logger.info('Training sample: {0}; Validation sample: {1}, Tesiting sample: {2}'.format( X_train.shape, X_val.shape, X_test.shape ))
    
    # train and test NN
    model = None
    
    if doTraining:
        # training
        logger.info('Train NN with {0} layers, {1} epochs, {2} batch size, {3} dropout. model tag is {4}'.format(layer_size_str,nepochs,batch_size,dropout,modeltag) )
 
        _, n_invars = X_train.shape
        model = get_model_nn(n_invars, layer_size, dropout=dropout)
        model.compile(optimizer='sgd',loss="binary_crossentropy",metrics=["accuracy"])
        #plot_model(model, to_file=graphfile, show_shapes=True)
        
        logger.info('Training ...')
        logger.info("{:>10}{:>10}".format("Epoch", "Loss"))

        # tensorboard
        callbacks = None
        if doTensorBoard:
            tb = TensorBoard(log_dir="logs/{0}_{1}".format(modeltag,time()), batch_size=batch_size, histogram_freq=1, write_graph=False, write_images=False, write_grads=False) if doTensorBoard else None
            callbacks = [tb]

        # gpu control
        # this works, but makes device busy and cannot stop ... reboot the machine
        #config = tf.ConfigProto()
        #config.gpu_options.allow_growth=True
        #sess = tf.Session(config=config)
        # 3tag: 250M / 8000M ~ 0.03125
        _gpu_mem_frac = None
        _sess = None
        if do3tag:
            _gpu_mem_frac = 0.03125
        else:
            _gpu_mem_frac = 0.03125
        if _gpu_mem_frac is None:
            pass
        else:
            _gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=_gpu_mem_frac)
            _sess = tf.Session(config=tf.ConfigProto(gpu_options=_gpu_options))
        
        _res = None
        # sample bkg per batch and use equval size sig bkg
        if doSampleBkg:
            generator_train = DataGenerator( 'train', X_train, Y_train, batch_size=batch_size, shuffle_per_epoch=True )
            generator_val = DataGenerator( 'validation', X_val, Y_val, batch_size=batch_size, shuffle_per_epoch=True )
            _nepoch_rec = generator_train.get_min_nepoch_recommended()
            if nepochs < _nepoch_rec:
                logger.info('Asked nepochs is smaller than what is calculated by bkg/sig sample size. USE latter {0} {1}'.format(nepochs,_nepoch_rec))
                nepochs = _nepoch_rec
            _res = model.fit_generator(generator=generator_train, validation_data=generator_val, use_multiprocessing=True, workers=4, epochs=nepochs, callbacks=callbacks, verbose=2) # no tensorboard connected use_multiprocessing=True, workers=4,
        # use all bkg
        else:
            _res = model.fit(X_train, Y_train, batch_size=batch_size, epochs=nepochs, validation_data=(X_test,Y_test), callbacks=callbacks, verbose=2)

        # now save the network
        logger.info('Save model to files {0}'.format(netfile))
        model.save(netfile)
        logger.info('Save history to files {0}'.format(file_performance))
        with open( file_performance, 'wb') as _handle:
            pickle.dump(_res.history, _handle, protocol=2) # back compatible with python2
    else:
        logger.info('No training, need model file in pwd: {0}'.format(netfile))
        model = load_model( netfile )

    # testing
    logger.info('Testing')
    Y_train_predict = model.predict(X_train)
    Y_test_predict = model.predict(X_test)
    Y_train_predict = np.squeeze(Y_train_predict) # (n,1) -> (n,)
    Y_test_predict = np.squeeze(Y_test_predict)
    bkg_rejection_vs_sig_efficiency = {}
    bkg_rejection_vs_sig_efficiency['train'] = bkg_rej_vs_sig_eff( Y_train, Y_train_predict ) # no weight for quick evaluation
    bkg_rejection_vs_sig_efficiency['test'] = bkg_rej_vs_sig_eff( Y_test, Y_test_predict ) # no weight for quick evaluation
    logger.info('Saving bkg_rej_vs_sig_eff to {0} WITHOUT weight'.format(file_roc))
    with open( file_roc, 'wb' ) as _handle:
        pickle.dump( bkg_rejection_vs_sig_efficiency, _handle, protocol=2) # back compatible with python2

    # quick plots
    # roc
    fig, ax = plt.subplots()
    plt.plot( bkg_rejection_vs_sig_efficiency['train'][1], bkg_rejection_vs_sig_efficiency['train'][0], 'b', label='training auc={0}'.format(auc(bkg_rejection_vs_sig_efficiency['train'][1], bkg_rejection_vs_sig_efficiency['train'][0])) )
    plt.plot( bkg_rejection_vs_sig_efficiency['test'][1], bkg_rejection_vs_sig_efficiency['test'][0], 'r--', label='testing auc={0}'.format(auc(bkg_rejection_vs_sig_efficiency['test'][1], bkg_rejection_vs_sig_efficiency['test'][0])) )
    plt.xlabel('Signal efficiency')
    plt.ylabel('Background rejection (1-bkg_eff)')
    legend = ax.legend(loc='lower left')
    plt.savefig('plot_roc_{0}.png'.format(modeltag))
    logger.info('Quick roc plot save to plot_roc_{0}.png'.format(modeltag))

    # nn output
    fig, ax = plt.subplots()
    n, bns, pth = plt.hist( Y_train_predict[ Y_train==1 ], 50, density=True, facecolor='r', alpha=0.30, label='Signal training')
    n, bns, pth = plt.hist( Y_train_predict[ Y_train==0 ], 50, density=True, facecolor='b', alpha=0.30, label='Background training')
    n, bns, pth = plt.hist( Y_test_predict[ Y_test==1 ], 50, density=True, linewidth=1.2, edgecolor='r', histtype='step', fill=False, label='Signal testing')
    n, bns, pth = plt.hist( Y_test_predict[ Y_test==0 ], 50, density=True, linewidth=1.2, edgecolor='b', histtype='step', fill=False, label='Background testing')
    plt.xlabel('NN output')
    plt.ylabel('Distribution normalised to unity')
    legend = ax.legend(loc='upper center')
    plt.savefig('plot_nnhist_{0}.png'.format(modeltag))
    logger.info('Quick nn output plot save to plot_nnhist_{0}.png'.format(modeltag))


    logger.info('All done')
    
    
if __name__ == '__main__':
  
  import config_algo_nn
  _config = getattr( config_algo_nn, sys.argv[1] )
  algo_nn( **_config )

