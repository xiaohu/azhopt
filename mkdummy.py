# dump a few events for quick tests of the framework ... debugging python scripts
import pickle
import numpy as np

myoption = 1

if myoption == 0:

  ### CONFIG ###
  thefile = '/eos/atlas/user/x/xiaohu/public/AZH_Optimisation/prepared-20181001_prefix-pNN_input_2tag_Sig_BKGs.pickle'
  ### END ###
  
  datafile = open( thefile, 'rb' )
  df = pickle.load( datafile )
  
  df = df[ (df.label == 306948) | (df.label == 306970) | (df.label == 306974) | ( ~df.IsSignal & (df.EventNumber < 30*1000) ) ]
  
  with open( 'dummy.pickle', 'wb') as savehandle:
    pickle.dump(df, savehandle, protocol=3)

if myoption == 1:

  ### CONFIG ###
  thefile = '/eos/user/x/xiaohu/AZH_EoR2/dataset/liverpool/20181001_prefix-pNN_input_2tag_Sig_BKGs.npy'
  ### END ###

  arr = np.load(thefile)
  arr_sel = arr[::1000]
  np.save('dummy.npy',arr_sel)


