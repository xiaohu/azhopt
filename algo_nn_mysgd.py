import argparse
import os
import re
from glob import glob
import numpy as np
import pandas as pd
import sys
from time import time

import pickle
from xutil import *
from phyutil import *
from mvautil import *

from sklearn.utils import shuffle
from sklearn.preprocessing import QuantileTransformer, MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score, roc_curve, auc
from sklearn.externals import joblib

from keras.models import Model, load_model
from keras.layers import Input, Dense, Dropout
from keras.regularizers import l1_l2
from keras.optimizers import SGD
from keras import backend as K
from keras.utils import plot_model
from keras.callbacks import TensorBoard

### CONFIG ###
#thefile = '/eos/user/x/xiaohu/AZH_EoR2/dataset/20181001_prefix-pNN_input_2tag_Sig_BKGs.add_variable.pickle'
thefile = 'test.pickle'
### END ###

do3tag = False
doTrainNetwork = True

layer_size = [50]
dropout = None
nepochs = 20
batch_size = 128

layer_size_str = 'n'.join( [ str(i) for i in layer_size] )
modeltag = 'nn_l{0}e{1}b{2}'.format(layer_size_str,nepochs,batch_size)

netfile = "model_{0}.h5".format(modeltag)
graphfile = "graph_{0}.png".format(modeltag)
file_performance = "perf_{0}.pickle".format(modeltag)
file_minmax_scaler = "minmax_scaler.pickle"
file_quantile_scaler = "quantile_scaler.pickle"


# system
logger = setup_custom_logger('algo_nn')

# load in data
logger.info('1. Load in dataset ...')
datafile = open( thefile, 'rb' )
df = pickle.load( datafile )
logger.info('load in done')
print(df)

# prepare training testing dataset
logger.info('2. Prepare training testing datasets')
logger.info(' + choose signal samples (may not all mA,mH)')
df = choose_signal_samples( df, [ 'ggFllbbmA400mH200', 'ggFllbbmA700mH200', 'ggFllbbmA700mH500' ] )
logger.info(' + generate random mA,mH for bkg')
sample_bkg_mass('mA', df, 1) # mH min/max mass is fixed in phyutil.py
sample_bkg_mass('mH', df, 1) # mA
logger.info(' + connect input variables depending on #bjets')
X = input_variables( do3tag, df )
logger.info(' + prepare target Y with type int')
Y = df.IsSignal.values.astype(int) # int for cost function
logger.info(' + shuffle all chosen entries')
X, Y = shuffle(X,Y)
logger.info(' + split sample for training 2/3, testing 1/3')
N = len(X)
separ = int(2*N/3)
X_train = X[:separ]
Y_train  = Y[:separ]
X_test = X[separ:]
Y_test = Y[separ:]
logger.info('Training sample: {0}; Tesiting sample: {1}'.format( len(X_train), len(X_test) ))

# train and test NN
model = None
minmax_trsf = None
quantile_trsf = None
fitres = []

if doTrainNetwork:
    logger.info('3. Train NN with {0} layers, {1} epochs, {2} batch size, {3} dropout. model tag is {4}'.format(layer_size_str,nepochs,batch_size,dropout,modeltag) )
    
    best_loss = 1e9
    min_delta = 0.0001
    patience = 25
    epochs_not_improved = 0
    # Reduce LR on plateau
    lr_best_loss = 1e9
    lr_patience = 10
    lr_factor = 0.5
    lr_epochs_not_improved = 0

    _, n_invars = X.shape
    model = get_model_nn(n_invars, layer_size, dropout=dropout)
    sgd = SGD(lr=0.1, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd,loss="binary_crossentropy",metrics=["accuracy"])
    plot_model(model, to_file=graphfile, show_shapes=True)
    # Preprocessing
    logger.info(' + preprocessing ...')
    quantile_trsf, minmax_trsf, X_train_trsf = preprocess(X_train)
    _,_, X_test_trsf = preprocess(X_test, quantile_trsf, minmax_trsf)

    logger.info(' + training ...')
    logger.info("{:>10}{:>10}".format("Epoch", "Loss"))
    _res = None
    tb = TensorBoard(log_dir="logs/{0}_{1}".format(layer_size_str,time()), batch_size=batch_size, histogram_freq=1, write_graph=True, write_images=True, write_grads=True)
    for epoch in range(nepochs):
        # sample_bkg_mass(X_train_trsf[:, -1], Y_train) # <--------- TEST IT
        _res = model.fit(X_train_trsf, Y_train, batch_size=batch_size, epochs=1, validation_data=(X_test_trsf,Y_test), verbose=0, callbacks=[tb])
        fitres.append(_res.history)
        loss_epoch = _res.history['loss'][0] # as nepoch is 1
        
        logger.info("{:>10.0f}{:>10.5f}".format(epoch, loss_epoch)) 
        # Reduce LR on plateau
        if loss_epoch + min_delta < lr_best_loss:
            lr_best_loss = loss_epoch
            lr_epochs_not_improved = 0
        else:
            lr_epochs_not_improved += 1
            
        # Early stopping
        if loss_epoch + min_delta < best_loss:
            best_loss = loss_epoch
            epochs_not_improved = 0
        else:
            epochs_not_improved += 1
        
        # Reduce LR on plateau
        if lr_epochs_not_improved > lr_patience:
            current_lr = K.get_value(model.optimizer.lr)
            logger.info("Current LR: {}".format(current_lr))
            logger.info("Reducing LR to: {}".format(lr_factor * current_lr))
            K.set_value(model.optimizer.lr, lr_factor * current_lr)
            lr_epochs_not_improved = 0

        # Early stopping
        if epochs_not_improved > patience:
            logger.info("Early stopping after epoch {}".format(epoch))
            break 
    # loop ends here

    # now save the network
    logger.info(' + save history of performance to file {0}'.format(file_performance))
    with open( file_performance, 'wb') as savehandle:
        pickle.dump(fitres, savehandle, protocol=3)
    logger.info(' + dump model and transformers to files {0} {1} {2}'.format(netfile,file_minmax_scaler,file_quantile_scaler))
    joblib.dump(minmax_trsf, file_minmax_scaler)
    joblib.dump(quantile_trsf, file_quantile_scaler)
    model.save(netfile)

    # print perfomance
    logger.info(' + print performance')
    logger.info('loss:{0}'.format(fitres[-1]['loss']))
    logger.info('loss validation:{0}'.format(fitres[-1]['val_loss']))
    logger.info('accuracy:{0}'.format(fitres[-1]['acc']))
    logger.info('accuracy validation:{0}'.format(fitres[-1]['val_acc']))

else:
    logger.info( "3. No traininng , directly application, read in the nn model and transformer files")
    logger.info(' + read in files {0} {1} {2}'.format(netfile,file_minmax_scaler,file_quantile_scaler))
    model = load_model(netfile)
    quantile_scaler = joblib.load(file_quantile_scaler)
    minmax_scaler = joblib.load(file_minmax_scaler)
    
# now it will do some testing
#logger.info('4. Testing')
#logger.info(' + transforming ...')
#_,_, X_train_trsf = preprocess(X_train, quantile_trsf, minmax_trsf)
#_,_, X_test_trsf = preprocess(X_test, quantile_trsf, minmax_trsf)
#
#logger.info(' + calculating prediction')
#results_train = model.predict(X_train_trsf)
#results_test = model.predict(X_test_trsf)
#
#fpr_test, tpr_test, thresholds_test = roc_curve(Y_test, results_test)
#auc_test = auc(fpr_test, tpr_test)
#
#fpr_train, tpr_train, thresholds_train = roc_curve(Y_train, results_train)
#auc_train = auc(fpr_train, tpr_train)
#
